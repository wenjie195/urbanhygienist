<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://urbanhygienist.com/cn" />
<meta property="og:title" content="告别过敏清洁服务 - 消除尘螨 | Urban Hygienist" />
<title>告别过敏清洁服务 - 消除尘螨 | Urban Hygienist</title>
<link rel="canonical" href="https://urbanhygienist.com/cn" />
<?php include 'css.php'; ?>
</head>

<body class="body">

<div id="overlay">
 <div class="center-food"><img src="img/loading-gif.gif" class="food-gif" alt="告别过敏清洁服务" title="告别过敏清洁服务"></div>
 <div id="progstat"></div>
 <div id="progress"></div>
</div>

    <div class="landing-first-div width100 same-padding cn-menu">
        <!-- Start Menu -->
        <header id="header" class="header header--fixed same-padding header1 menu-white index-menu" role="banner">
            <div class="big-container-size hidden-padding">
                <div class="left-logo-div float-left hidden-logo-padding">
                    <img src="img/urban-hygienist.png" class="logo-img" alt="Urban Hygienist" title="Urban Hygienist">
                </div>
                
                <div class="right-menu-div float-right" id="top-menu">
                    <a href="#about" class="green-text-a menu-padding">关于我们</a>
                    <a href="#services" class="green-text-a menu-padding">清洁服务</a>
                    <a href="#whyus" class="green-text-a menu-padding">备受承认</a>
                    <a href="#testi" class="green-text-a menu-padding">顾客评价</a>
                    <a href="#contact" class="green-text-a menu-padding">联络我们</a>
                    <a href="index.php" class="green-text-a menu-padding" style="font-weight:500;">English</a>
                    <a href="login.php" class="green-text-a menu-padding">登入</a>
                    <a class="open-menu menu-icon"><img src="img/menu.png" class="menu-img"></a>
                <!-- Mobile View-->
                <!--    <a href="#whatyouneed" class="white-text menu-padding red-hover2">
                        <img src="img/thousand-media/menu-icon-14.png" class="menu-img" alt="Your Need" title="Your Need">
                    </a>
                    <a href="#promotion" class="white-text menu-padding red-hover2">
                        <img src="img/thousand-media/menu-icon-13.png" class="menu-img" alt="Promotion" title="Promotion">
                    </a>
        
                    <a href="#services" class="white-text menu-padding red-hover2">
                        <img src="img/thousand-media/menu-icon-11.png" class="menu-img" alt="Services" title="Services">            
                    </a>
                    <a href="#contact" class="white-text menu-padding red-hover2">
                        <img src="img/thousand-media/menu-icon-10.png" class="menu-img" alt="Contact" title="Contact">            
                    </a>
                    <a class="white-text menu-padding red-hover2 open-register pointer">
                        <img src="img/thousand-media/menu-icon-15.png" class="menu-img" alt="Register" title="Register">            
                    </a>
                    <a class="white-text red-hover2 open-login pointer">
                        <img src="img/thousand-media/menu-icon-16.png" class="menu-img" alt="Login" title="Login">            
                    </a>        -->    
                </div>
            </div>
        
        </header>
        <h1 class="dark-text h1-title first-h1">没螨生活，从现在开始。</h1>
        <p class="h1-p dark-text2">我们将协助您脱离此烦恼。</p>
        <a class="open-contact"><div class="white-button hover-white open-contact first-white-div">获取免费清洗床垫服务</div></a>
    
    
    </div>
<div id="about">    
    <div class="carousel-div">
        <section class="single-item">
            <div>
            	<div class="img-div">
              		<img src="img/vivenso.png" class="carousel-img" alt="Vivenso - Multifunction Cleaning System" title="Vivenso - Multifunction Cleaning System">
                    <p class="product-p text-center">Vivenso 多功能清洗系统</p>
            	</div>
            </div>
            <div>
            	<div class="img-div">
            		<img src="img/airwasher.png" class="carousel-img" alt="Venta - Air Washer" title="Venta - Air Washer">
                    <p class="product-p text-center">Venta 空气清洗器</p>
                </div>
            </div>
            <div>
            	<div class="img-div">
            		<img src="img/air-freshener.png" class="carousel-img" alt="Airvenue - Air Freshener" title="Airvenue - Air Freshener">
                    <p class="product-p text-center">Airvenue 空气清新器</p>
                </div>
            </div>            
        </section>    
    </div>
    <div class="clear"></div>
    <div class="width100 same-padding cleanliness text-center margin-auto">
    	<p class="sub-title dark-text">你和家人，需要一个真正洁净的家！</p>
        <p class="normal-content dark-text margin-auto">Urban Hygienist 为您提供有效的清洁方案，通过环保的净水技术去除家里的灰尘、尘螨和过敏原，从而控制湿疹、过敏性鼻炎和哮喘病情。
</p>
    </div>
    <div class="animated-div same-padding margin-auto">
    	<img src="img/dining.gif" class="animated-img" alt="无过敏原环境" title="无过敏原环境">
    	<p class="animated-p text-center">在无过敏原环境中享受天伦之乐。</p>
    </div>
    <div class="animated-div same-padding margin-auto">
    	<img src="img/sleep2.gif" class="animated-img" alt="睡眠不再被过敏原和哮喘引起的呼吸问题干扰。" title="睡眠不再被过敏原和哮喘引起的呼吸问题干扰。">
    	<p class="animated-p text-center">睡眠不再被过敏原和哮喘引起的呼吸问题干扰。</p>
    </div>    
    <div class="animated-div same-padding margin-auto">
    	<img src="img/living3.gif" class="animated-img" alt="清洁健康的家居" title="清洁健康的家居">
    	<p class="animated-p text-center">减少家务量的同时，增加与家人在清洁健康的家居里的相处时光。</p>
    </div>    

    <div class="animated-div same-padding margin-auto">
    	<iframe class="animated-img iframe-video" src="https://www.youtube.com/embed/b9C9mBYh0ig" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div> 

    
</div>
<div class="grey-bg">
    <div id="services">
        <div class="clear"></div>
        <div class="width100 same-padding distance">
            <div class="width100 text-center">
                <img src="img/our-service.png" class="text-center three-img--top service-img" alt="Urban Hygienist 服务" title="Urban Hygienist 服务">
                <p class="sub-title dark-text text-center">我们的服务</p>
            </div>
            <div class="clear"></div>
            <div class="three-div text-center hover-a open-mattress">
                <img src="img/mattress-cleaning.png" class="three-img" alt="清洗床垫" title="清洗床垫">
                <p class="desc-p dark-text text-center">清洗床垫</p>
            </div>
            <div class="three-div text-center middle-three second-three hover-a open-air">
                <img src="img/room-air-cleaning.png" class="three-img" alt="净化空气" title="净化空气">
                <p class="desc-p dark-text text-center">净化空气</p>
            </div>
            <div class="three-div text-center hover-a open-curtain">
                <img src="img/curtain-cleaning.png" class="three-img" alt="清洗窗帘" title="清洗窗帘">
                <p class="desc-p dark-text text-center">清洗窗帘</p>
            </div>    
            <div class="three-div text-center second-three hover-a open-carpet">
                <img src="img/carpet-cleaning.png" class="three-img" alt="清洗沙发" title="清洗沙发">
                <p class="desc-p dark-text text-center">清洗沙发</p>
            </div> 
            <div class="three-div text-center middle-three hover-a open-sofa">
                <img src="img/sofa-cleaning.png" class="three-img" alt="清洗地板和地毯" title="清洗地板和地毯">
                <p class="desc-p dark-text text-center">清洗地板和地毯</p>
            </div>        
            <div class="three-div text-center second-three hover-a open-floor">
                <img src="img/maid-service.png" class="three-img" alt="女佣服务" title="女佣服务">
                <p class="desc-p dark-text text-center">女佣服务</p>
            </div>                    
        </div>
    </div>

    
        <div class="clear"></div>
        <div class="width100 same-padding distance2">
            <div class="width100 text-center" id="whyus">
                <img src="img/why-us.png" class="text-center three-img--top" alt="我们备受承认的品质" title="我们备受承认的品质">
                <p class="sub-title dark-text text-center">我们备受承认的品质</p>
            </div>
            <div class="three-div2">
             	<a href="./img/plus-x-award.png" data-fancybox title="plus x award">
                	<img src="img/award-winning-german-technology-cn.png" class="width100 opacity-hover" alt="获奖德国科技技术奖项" title="获奖德国科技技术奖项">
                </a>           
            </div>
            <div class="three-div2 middle-three-2">
            	<a href="./img/cert1.png" 
                     data-fancybox="images-preview"  title="Quality Seal for Allergy-Friendly Products and Services"
                     >
                	<img src="img/certified-preventative-medical-product-cn.png" class="width100 opacity-hover" alt="已被认证为适合过敏族的预防过敏医疗产品" title="已被认证为适合过敏族的预防过敏医疗产品">
                </a>
                <div style="display: none;">
                 
                
                  <a href="./img/cert2.png" data-fancybox="images-preview" title="Produktsiegel Certificate Allergiker-geeignet" ></a>
                
                  <a href="./img/cert3.png" data-fancybox="images-preview" title="Produktsiegel Certificate Allergiker-geeignet" ></a>
                
                  <a href="./img/cert4.png" data-fancybox="images-preview" title="Health Product - Room Cleaning System"></a>
                  <a href="./img/cert5.png" data-fancybox="images-preview" title="Health Product - Air Refresher"></a>
                </div>            
            

            </div>
            <div class="three-div2">
                <img src="img/10-year-guarantee-cn.png" class="width100" alt="Up to 10 years guarantee*" title="长达10年的产品保修*">
            </div>                   
        </div>

        <div class="clear"></div>
        <div class="width100 same-padding distance2 urban-bg" id="testi">
            <div class="width100 text-center">
                <img src="img/review2.png" class="text-center three-img--top" alt="Why Urban Hygienist" title="Why Urban Hygienist">
                <p class="sub-title dark-text text-center test-p">客户评价</p>
                <p class="product-p text-center">了解客户对我们的评价。</p>
                <iframe class="review-iframe iframe1"  src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Furbanhygienist%2F&tabs&width=500&height=214&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=2101026363514817" width="500" height="214" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                <div class="clear"></div>
                <iframe class="review-iframe iframeb1" src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Flmemooi%2Fposts%2F10155785927606344%3A0&width=500" width="500" height="335" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
				<div class="clear"></div>
                <iframe class="review-iframe iframeb2"  src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fjune.arcn%2Fposts%2F2077493252376261&width=500" width="500" height="161" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                <div class="clear"></div>
                <iframe  class="review-iframe iframeb3" src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fah.bee.73%2Fposts%2F2438873162804226&width=500" width="500" height="161" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                <div class="clear"></div>
                <iframe class="review-iframe iframeb4" src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fchan.hiokzhee.50%2Fposts%2F438432493297748%3A0&width=500" width="500" height="316" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                <div class="clear"></div>
                <iframe class="review-iframe iframeb5"  src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fjojo.lee.106%2Fposts%2F10218857103166755&width=500" width="500" height="199" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                <div class="clear"></div>
                <iframe class="review-iframe iframeb6" src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fsusan.ng.5437%2Fposts%2F2118110598247685&width=500" width="500" height="161" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                <div class="clear"></div>
                <iframe class="review-iframe iframeb7" src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fpermalink.php%3Fstory_fbid%3D1469552663188610%26id%3D100004016725565&width=500" width="500" height="180" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                <div class="clear"></div>
                <iframe class="review-iframe iframeb8" src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fgigitang.twc%2Fposts%2F10212784922873748&width=500" width="500" height="617" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                <div class="clear"></div>
                
            </div>        
      </div>        
  
        
        <div class="clear"></div>
        <div class="last-div width100 same-padding distance text-center">
        	<div id="contact">
                <h1 class="h1-title last-h1 text-center">现在就为了您的家人改善家居清洁吧！</h1>
                <p class="text-center"><a class="open-contact hover-white"><div class="white-button margin-auto open-contact contact-white-button">获取免费清洗床垫服务</div></a></p>
 				<p class="text-center social-media-p">
                	<a class="opacity-hover" target="_blank" href="https://www.facebook.com/urbanhygienist/"><img src="img/facebook.png" class="social-icon social-icon1" alt="Urban Hygienist Facebook" title="Urban Hygienist Facebook"></a>
                	<a class="opacity-hover" target="_blank" href="https://www.instagram.com/myurbanhygienist/"><img src="img/instagram.png" class="social-icon social-icon2" alt="Urban Hygienist Instagram" title="Urban Hygienist Instagram"></a>
                </p>
            </div>
            
        </div>
   

</div>

<div class="clear"></div>
<div class="uh-footer width100 same-padding white-text text-center">
	© 2020 Urban Hygienist, 版权所有。
</div>

<!-- Contact Modal Box -->
<div id="modal-contact" class="modal">
  <div class="modal-content">
    <span class="close-contact close-css">&times;</span>
    			<h2 class="contact-h2 dark-text">填写获取礼品卷</h2>
                <form id="contactform" method="post" action="cn.php" class="form-class extra-margin">
                  <p class="form-p dark-text">名字</p>
                  <input type="text" name="name" placeholder="输入您的名字" class="input-name clean" required >
                  <p class="form-p dark-text">联络号码</p>
                  <input type="text" name="telephone" placeholder="输入您的联络号码" class="input-name clean" required >

                  <p class="form-p dark-text">地址</p>
                  <input type="text" name="address" placeholder="输入您的地址" class="input-name clean" > 
                  <p class="form-p dark-text">婚姻状况</p> 
                  <select class="clean input-name input-select" name="status">
                  	<option class="clean input-name input-option">-</option>
                  	<option class="clean input-name input-option">未婚</option>
                    <option class="clean input-name input-option">已婚</option>
                  </select>
                  <p class="form-p dark-text">年龄</p> 
                  <select class="clean input-name input-select" name="age">
                  <option class="clean input-name input-option">-</option>
                  	<option class="clean input-name input-option">20-29</option>
                    <option class="clean input-name input-option">30-40</option>
                    <option class="clean input-name input-option">41-50</option>
                    <option class="clean input-name input-option">51-60</option>
                    <option class="clean input-name input-option">61-70</option>
                    <option class="clean input-name input-option">> 70</option>
                    <option class="clean input-name input-option">< 20</option>
                  </select>
                  <p class="form-p dark-text">您或您的家人有以下任何症状吗？</p> 
                  <select class="clean input-name input-select" name="conditions">
                  	<option class="clean input-name input-option">-</option>
                  	<option class="clean input-name input-option">无</option>
                  	<option class="clean input-name input-option">哮喘</option>
                    <option class="clean input-name input-option">鼻窦炎</option>
                    <option class="clean input-name input-option">皮肤敏感</option>
                    <option class="clean input-name input-option">其他</option>
                  </select> 
                  <p class="form-p dark-text">如果您选择其他，请注明该症状：</p> 
                  <textarea placeholder="Other Medical Conditions" class="input-name input-message clean" name="comments"  ></textarea>                                   
                  <p class="form-p dark-text">较喜欢预约落在</p> 
                  <select class="clean input-name input-select" name="date">
                  	<option class="clean input-name input-option">-</option>
                  	<option class="clean input-name input-option">周一至周五</option>
                    <option class="clean input-name input-option">周末</option>
                  </select> 
                  <div class="clear"></div>
                   
                  <div class="res-div"><input type="submit" name="submit" value="提交并获取礼品卷" class="input-submit white-text clean pointer"  onClick="myFunction5()"></div>
                </form> 
  </div>
</div>


<!-- Menu Modal Box -->
<div id="modal-menu" class="modal menu-modal-css">
  <div class="modal-content menu-modal-content">
    <span class="close-menu close-css">&times;</span>
		<h2 class="menu-h2 text-center dark-text">目录</h2>
        <p class="menu-p text-center"><a href="#about" class="menu-p-a green-text-a close-menu">关于我们</a></p>
        <p class="menu-p text-center"><a href="#services" class="menu-p-a green-text-a close-menu">清洁服务</a></p>
        <p class="menu-p text-center"><a href="#whyus" class="menu-p-a green-text-a close-menu">备受承认</a></p> 
        <p class="menu-p text-center"><a href="#testi" class="menu-p-a green-text-a close-menu">顾客评价</a></p>
        <p class="menu-p text-center"><a href="#contact" class="menu-p-a green-text-a close-menu">联络我们</a></p>
        <p class="menu-p text-center"><a href="./" class="menu-p-a green-text-a close-menu">Switch to English</a></p>
        <p class="menu-p text-center"><a href="login.php" class="menu-p-a green-text-a close-menu">登入</a></p>                               				
  </div>
</div>


<!-- Mattress Modal Box -->
<div id="modal-mattress" class="modal mattress-modal-css">
  <span class="close-mattress close-mattress2 close-css">&times;</span>
  <span class="arrow-span left-arrow open-floor"><img src="img/prev.png" class="arrow-img"></span>
  <span class="arrow-span right-arrow open-air"><img src="img/next.png" class="arrow-img"></span>
  <div class="modal-content mattress-modal-content">
    	
		<div class="modal-left-div mattress-bg"></div>
        <div class="modal-right-div">
        	<h2 class="service-title">清洗床垫</h2>
            <ul class="service-ul">
            	<li class="service-li">我们的床和枕头是尘螨和细菌最喜欢的聚会场所之一。在我们睡着的时候，尘螨会出来“聚餐”，在我们的皮肤上享用死皮。然而，引起瘙痒和过敏反应的起因并不是因为害虫本身，而是它们遗留下来的粪便。</li>
                 <li class="service-li">人类一生中平均超过三分之一的时间都在睡觉。你是否能想象尘螨在没有深层清洗的床垫的情况下举办了多少个的“聚餐会”？</li>
                <li class="service-li">专家建议大家至少每3个月清洗一次床垫，以便您和您的家人能拥有健康和良好的睡眠品质。</li>
            </ul>
        </div>                     				
  </div>
</div>


<!-- Room Air Cleaning Modal Box -->
<div id="modal-air" class="modal mattress-modal-css">
  <span class="close-air close-mattress2 close-css">&times;</span>
  <span class="arrow-span left-arrow open-mattress"><img src="img/prev.png" class="arrow-img"></span>
  <span class="arrow-span right-arrow open-curtain"><img src="img/next.png" class="arrow-img"></span>
  <div class="modal-content mattress-modal-content">
    	
		<div class="modal-left-div room-bg"></div>
        <div class="modal-right-div">
        	<h2 class="service-title">净化空气</h2>
            <ul class="service-ul">
            	<li class="service-li">地上的污迹和布满橱柜的灰尘都在提醒着我们是时候该清理了。但是，您又如何分辨家中的空气是时候该净化了呢？</li>
                <li class="service-li">您是否知道我们的床、窗帘、沙发和地毯上积聚的细菌和灰尘会让室内空气比起室外空气污染指数高出5倍？人类平均90％的时间在室内度过，吸进的空气大部分都在室内，而长期吸入将会严重损害健康。</li>
                <li class="service-li">虽然我们经常关闭门窗以防止屋外的污染空气进入;然而，我们也在防止室内空气的毒素排出室外！但是，别担心！Urban Hygienist可以轻松解决这问题。</li>
            </ul>
        </div>                     				
  </div>
</div>


<!-- Curtain Cleaning Modal Box -->
<div id="modal-curtain" class="modal mattress-modal-css">
  <span class="close-curtain close-mattress2 close-css">&times;</span>
  <span class="arrow-span left-arrow open-air"><img src="img/prev.png" class="arrow-img"></span>
  <span class="arrow-span right-arrow open-carpet"><img src="img/next.png" class="arrow-img"></span>
  <div class="modal-content mattress-modal-content">
    	
		<div class="modal-left-div curtain-bg"></div>
        <div class="modal-right-div">
        	<h2 class="service-title">清洗窗帘</h2>
            <ul class="service-ul">
            	<li class="service-li">大多数人在打扫房屋时往往都会忘记清理窗帘。可是你知道窗帘如果长期被忽略，将会对您的健康造成严重伤害，得不偿失！</li>
                <li class="service-li">您是否有留意到每当您打开窗帘时，都会有小小的飞行物体在空中飘来飘去？这些都是会引起过敏反应和哮喘问题的起因。由于它们体积太小，很容易被风携带，并积聚在窗帘上。</li>
                <li class="service-li">窗帘就好像我们家的盾牌，它可以遮挡高温、寒冷、阳光直射和污垢。Urban Hygienist将确保您家中的第一道防线无过敏原的烦恼。</li>
            </ul>
        </div>                     				
  </div>
</div>


<!-- Carpet Cleaning Modal Box -->
<div id="modal-carpet" class="modal mattress-modal-css">
  <span class="close-carpet close-mattress2 close-css">&times;</span>
  <span class="arrow-span left-arrow open-curtain"><img src="img/prev.png" class="arrow-img"></span>
  <span class="arrow-span right-arrow open-sofa"><img src="img/next.png" class="arrow-img"></span>
  <div class="modal-content mattress-modal-content">
    	
		<div class="modal-left-div carpet-bg"></div>
        <div class="modal-right-div">
        	<h2 class="service-title">清洗地板和地毯</h2>
            <ul class="service-ul">
            	<li class="service-li">我们并不是想要吓唬您，但您是否知道室内地毯比马桶座肮脏近4000倍，每平方英寸约有200,000个细菌？</li>
                <li class="service-li">当室外的污迹和细菌落入地毯纤维中时，地毯就会变成细菌、灰尘、花粉和死皮颗粒的窝巢。当这些过敏原飞到空中时，它们会引发呼吸问题和喘息。</li>
                <li class="service-li">Urban Hygienist的专业人员将确保您的家人不会再受到这些过敏原的威胁。</li>
            </ul>
        </div>                     				
  </div>
</div>

<!-- Sofa Cleaning Modal Box -->
<div id="modal-sofa" class="modal mattress-modal-css">
  <span class="close-sofa close-mattress2 close-css">&times;</span>
  <span class="arrow-span left-arrow open-carpet"><img src="img/prev.png" class="arrow-img"></span>
  <span class="arrow-span right-arrow open-floor"><img src="img/next.png" class="arrow-img"></span>
  <div class="modal-content mattress-modal-content">
    	
		<div class="modal-left-div sofa-bg"></div>
        <div class="modal-right-div">
        	<h2 class="service-title">清洗沙发</h2>
            <ul class="service-ul">
            	<li class="service-li">经过工作忙碌的一天，我们一回到家里就会躺在沙发上，观看喜爱的电视节目或滑手机。通常我们都不会先洗澡或换上干净的衣服再躺在沙发，而沙发就成了我们回到家时第一个接触到的东西。</li>
                <li class="service-li">在我们享受的同时，衣服上残留的细菌和过敏原都会掉落在沙发上。</li>
                <li class="service-li">通过专业的深层清洁服务可以维护您的沙发清洁和摆脱污染物带来的祸害。</li>
            </ul>
        </div>                     				
  </div>
</div>

<!-- Maid Modal Box -->
<div id="modal-floor" class="modal mattress-modal-css">
  <span class="close-floor close-mattress2 close-css">&times;</span>
  <span class="arrow-span left-arrow open-sofa"><img src="img/prev.png" class="arrow-img"></span>
  <span class="arrow-span right-arrow open-mattress"><img src="img/next.png" class="arrow-img"></span>
  <div class="modal-content mattress-modal-content">
    	
		<div class="modal-left-div maid-bg"></div>
        <div class="modal-right-div">
        	<h2 class="service-title">女佣服务</h2>
            <ul class="service-ul">
            	<li class="service-li">如果您想要放松身心，让其他人替您清理家居，Urban Hygienist将会是你的首选！我们的专业清洁团队将确保您的房屋清理后一尘不染。</li>
            </ul>
        </div>                     				
  </div>
</div>
<style>
body{
	font-size:120%;}
@media all and (max-width: 700px){
.menu-padding{
	font-size:15px !important;}
}
@media all and (max-width: 580px){
.menu-padding{
	font-size:14px !important;
	margin-left: 10px;}
}
@media all and (max-width: 500px){
.menu-padding{
	font-size:13px !important;
	margin-left: 10px;}
}
</style>
<script src="js/jquery-3.3.1.min.js" type="text/javascript"></script> 
<script src="js/bootstrap.min.js" type="text/javascript"></script>  
<script src="js/jquery.fancybox.js" type="text/javascript"></script>  
<script src="./slick/slick.js" type="text/javascript" charset="utf-8"></script>  
<script>
	$('.single-item').slick();
</script>
<script src="js/headroom.js"></script>
<script>
    $(window).load(function(){
       // PAGE IS FULLY LOADED  
       // FADE OUT YOUR OVERLAYING DIV
       $('#overlay').fadeOut();
    });
</script>
<script>
    ;(function(){
      function id(v){return document.getElementById(v); }
      function loadbar() {
        var ovrl = id("overlay"),
            prog = id("progress"),
            stat = id("progstat"),
            img = document.images,
            c = 0;
            tot = img.length;
    
        function imgLoaded(){
          c += 1;
          var perc = ((100/tot*c) << 0) +"%";
          prog.style.width = perc;
          stat.innerHTML = "Loading "+ perc;
          if(c===tot) return doneLoading();
        }
        function doneLoading(){
          ovrl.style.opacity = 0;
          setTimeout(function(){ 
            ovrl.style.display = "none";
          }, 1200);
        }
        for(var i=0; i<tot; i++) {
          var tImg     = new Image();
          tImg.onload  = imgLoaded;
          tImg.onerror = imgLoaded;
          tImg.src     = img[i].src;
        }    
      }
      document.addEventListener('DOMContentLoaded', loadbar, false);
    }());
</script>
<script>
    (function() {
        var header = new Headroom(document.querySelector("#header"), {
            tolerance: 5,
            offset : 205,
            classes: {
              initial: "animated",
              pinned: "slideDown",
              unpinned: "slideUp"
            }
        });
        header.init();
    
    }());
</script>
 
	<script>
    // Cache selectors
    var lastId,
        topMenu = $("#top-menu"),
        topMenuHeight = topMenu.outerHeight(),
        // All list items
        menuItems = topMenu.find("a"),
        // Anchors corresponding to menu items
        scrollItems = menuItems.map(function(){
          var item = $($(this).attr("href"));
          if (item.length) { return item; }
        });
    
    // Bind click handler to menu items
    // so we can get a fancy scroll animation
    menuItems.click(function(e){
      var href = $(this).attr("href"),
          offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
      $('html, body').stop().animate({ 
          scrollTop: offsetTop
      }, 500);
      e.preventDefault();
    });
    
    // Bind to scroll
    $(window).scroll(function(){
       // Get container scroll position
       var fromTop = $(this).scrollTop()+topMenuHeight;
       
       // Get id of current scroll item
       var cur = scrollItems.map(function(){
         if ($(this).offset().top < fromTop)
           return this;
       });
       // Get the id of the current element
       cur = cur[cur.length-1];
       var id = cur && cur.length ? cur[0].id : "";
                      
    });
    </script>
	
    
<script>
	//Contact
	var modalContact = document.getElementById("modal-contact");
	

	var btnContact1 = document.getElementsByClassName("open-contact")[0];
	var btnContact2 = document.getElementsByClassName("open-contact")[1];	
	var btnContact3 = document.getElementsByClassName("open-contact")[2];
	var btnContact4 = document.getElementsByClassName("open-contact")[3];	

	var spanContact = document.getElementsByClassName("close-contact")[0];

	//Menu	
	var modalMenu = document.getElementById("modal-menu");
	

	var btnMenu = document.getElementsByClassName("open-menu")[0];

	var spanMenu1 = document.getElementsByClassName("close-menu")[0];
	var spanMenu2 = document.getElementsByClassName("close-menu")[1];
	var spanMenu3 = document.getElementsByClassName("close-menu")[2];
	var spanMenu4 = document.getElementsByClassName("close-menu")[3];
	var spanMenu5 = document.getElementsByClassName("close-menu")[4];			
	var spanMenu6 = document.getElementsByClassName("close-menu")[5];	
	var spanMenu7 = document.getElementsByClassName("close-menu")[6];
	//Mattress	
	var modalMattress = document.getElementById("modal-mattress");
	

	var btnMattress = document.getElementsByClassName("open-mattress")[0];
	var btnMattress2 = document.getElementsByClassName("open-mattress")[1]; //Room Air Cleaing arrow
	var btnMattress3 = document.getElementsByClassName("open-mattress")[2];
	
	var spanMattress = document.getElementsByClassName("close-mattress")[0];


	//Room Air Cleaning	
	var modalAir = document.getElementById("modal-air");
	

	var btnAir = document.getElementsByClassName("open-air")[0]; 
	var btnAir2 = document.getElementsByClassName("open-air")[1]; //Mattress Arrow
	var btnAir3 = document.getElementsByClassName("open-air")[2]; //Curtain Arrow
	
	var spanAir = document.getElementsByClassName("close-air")[0];	
	


	//Curtain Cleaning	
	var modalCurtain = document.getElementById("modal-curtain");
	

	var btnCurtain = document.getElementsByClassName("open-curtain")[0]; 
	var btnCurtain2 = document.getElementsByClassName("open-curtain")[1]; //Air Arrow
	var btnCurtain3 = document.getElementsByClassName("open-curtain")[2]; //Carpet Arrow
	
	var spanCurtain = document.getElementsByClassName("close-curtain")[0];		


	//Carpet Cleaning	
	var modalCarpet = document.getElementById("modal-carpet");
	

	var btnCarpet = document.getElementsByClassName("open-carpet")[0]; 
	var btnCarpet2 = document.getElementsByClassName("open-carpet")[1]; 
	var btnCarpet3 = document.getElementsByClassName("open-carpet")[2]; 
	
	var spanCarpet = document.getElementsByClassName("close-carpet")[0];

	//Sofa Cleaning	
	var modalSofa = document.getElementById("modal-sofa");
	

	var btnSofa = document.getElementsByClassName("open-sofa")[0]; 
	var btnSofa2 = document.getElementsByClassName("open-sofa")[1]; 
	var btnSofa3 = document.getElementsByClassName("open-sofa")[2]; 
	
	var spanSofa = document.getElementsByClassName("close-sofa")[0];		

	//Maid Service	
	var modalFloor = document.getElementById("modal-floor");
	

	var btnFloor = document.getElementsByClassName("open-floor")[0]; 
	var btnFloor2 = document.getElementsByClassName("open-floor")[1]; 
	var btnFloor3 = document.getElementsByClassName("open-floor")[2]; 
	
	var spanFloor = document.getElementsByClassName("close-floor")[0];	

	

	btnContact1.onclick = function() {
	  modalContact.style.display = "block";
	}
	btnContact2.onclick = function() {
	  modalContact.style.display = "block";
	}	
	btnContact3.onclick = function() {
	  modalContact.style.display = "block";
	}
	btnContact4.onclick = function() {
	  modalContact.style.display = "block";
	}
	btnMenu.onclick = function() {
	  modalMenu.style.display = "block";
	}
	btnMattress.onclick = function() {
	  modalMattress.style.display = "block";
	}
	btnMattress2.onclick = function() {
	  modalAir.style.display = "none";
	  modalMattress.style.display = "block";
	}
	btnMattress3.onclick = function() {
	  modalFloor.style.display = "none";
	  modalMattress.style.display = "block";
	}	
	btnAir.onclick = function() {
	  modalAir.style.display = "block";	  
	}	
	btnAir2.onclick = function() {
	  modalMattress.style.display = "none";
	  modalAir.style.display = "block";	  
	}
	btnAir3.onclick = function() {
	  modalCurtain.style.display = "none";
	  modalAir.style.display = "block";	  
	}
	btnCurtain.onclick = function() {
	  modalCurtain.style.display = "block";	  
	}	
	btnCurtain2.onclick = function() {
	  modalAir.style.display = "none";
	  modalCurtain.style.display = "block";	  
	}
	btnCurtain3.onclick = function() {
	  modalCarpet.style.display = "none";
	  modalCurtain.style.display = "block";	  
	}
	btnCarpet.onclick = function() {
	  modalCarpet.style.display = "block";	  
	}	
	btnCarpet2.onclick = function() {
	  modalCurtain.style.display = "none";
	  modalCarpet.style.display = "block";	  
	}
	btnCarpet3.onclick = function() {
	  modalSofa.style.display = "none";
	  modalCarpet.style.display = "block";	  
	}
	btnSofa.onclick = function() {
	  modalSofa.style.display = "block";	  
	}	
	btnSofa2.onclick = function() {
	  modalCarpet.style.display = "none";
	  modalSofa.style.display = "block";	  
	}
	btnSofa3.onclick = function() {
	  modalFloor.style.display = "none";
	  modalSofa.style.display = "block";	  
	}
	btnFloor.onclick = function() {
	  modalFloor.style.display = "block";	  
	}	
	btnFloor2.onclick = function() {
	  modalMattress.style.display = "none";
	  modalFloor.style.display = "block";	  
	}
	btnFloor3.onclick = function() {
	  modalSofa.style.display = "none";
	  modalFloor.style.display = "block";	  
	}


	spanContact.onclick = function() {
	  modalContact.style.display = "none";
	}
	spanMenu1.onclick = function() {
	  modalMenu.style.display = "none";
	}
	spanMenu2.onclick = function() {
	  modalMenu.style.display = "none";
	}
	spanMenu3.onclick = function() {
	  modalMenu.style.display = "none";
	}
	spanMenu4.onclick = function() {
	  modalMenu.style.display = "none";
	}
	spanMenu5.onclick = function() {
	  modalMenu.style.display = "none";
	}	
	spanMenu6.onclick = function() {
	  modalMenu.style.display = "none";
	}	
	spanMenu7.onclick = function() {
	  modalMenu.style.display = "none";
	}			
	spanMattress.onclick = function() {
	  modalMattress.style.display = "none";
	}
	spanAir.onclick = function() {
	  modalAir.style.display = "none";
	}
	spanCurtain.onclick = function() {
	  modalCurtain.style.display = "none";
	}
	spanCarpet.onclick = function() {
	  modalCarpet.style.display = "none";
	}
	spanSofa.onclick = function() {
	  modalSofa.style.display = "none";
	}
	spanFloor.onclick = function() {
	  modalFloor.style.display = "none";
	}

	window.onclick = function(event) {
	  if (event.target == modalContact) {
		modalContact.style.display = "none";
	  }
	  if (event.target == modalMenu) {
		modalMenu.style.display = "none";
	  }	  
	  if (event.target == modalMattress) {
		modalMattress.style.display = "none";
	  }	
	  if (event.target == modalAir) {
		modalAir.style.display = "none";
	  }	
	  if (event.target == modalCurtain) {
		modalCurtain.style.display = "none";
	  }		  
	  if (event.target == modalCarpet) {
		modalCarpet.style.display = "none";
	  }	
	  if (event.target == modalSofa) {
		modalSofa.style.display = "none";
	  }		
	  if (event.target == modalFloor) {
		modalFloor.style.display = "none";
	  }		    	  	  	  
	}
</script>

<script>
function myFunction5() {
  window.open("https://urbanhygienist.com/img/Urban-Hygienist-Free-Mattress-Cleaning-eVoucher.pdf", "_blank", "Download");
}
</script>



 
<!-- CSS -->
<style>
.food-gif{
	width:100px;
	position:absolute;
	top:calc(50% - 150px);
	text-align:center;
}
.center-food{
	width:100%;
	text-align:center;
	margin-left:-50px;}
#container{
	margin-top:-20px;}
#overlay{
  position:fixed;
  z-index:99999;
  top:0;
  left:0;
  bottom:0;
  right:0;
  background:#7cd1d1;
  /*background: -moz-linear-gradient(left, #a9151c 0%, #d60d26 100%);
  background: -webkit-linear-gradient(left, #a9151c 0%,#d60d26 100%);
  background: linear-gradient(to right, #a9151c 0%,#d60d26 100%);*/
  transition: 1s 0.4s;
}
#progress{
  height:1px;
  background:#fff;
  position:absolute;
  width:0;
  top:50%;
}
#progstat{
  font-size:0.7em;
  letter-spacing: 3px;
  position:absolute;
  top:50%;
  margin-top:-40px;
  width:100%;
  text-align:center;
  color:#fff;
}

@media all and (max-width: 500px){
.food-gif{
	width:60px;
	top:calc(50% - 120px);
	text-align:center;
}
.center-food{
	margin-left:-30px;}	

}
</style>
<!--gwendolynkoay.vidatech@gmail.com,sherry2.vidatech@gmail.com,kevinyam.vidatech@gmail.com,urbanhygienist@gmail.com-->
<?php

if( $_SERVER['REQUEST_METHOD'] == 'POST') {

    // EDIT THE 2 LINES BELOW AS REQUIRED
    $email_to = "urbanhygienisttelemarketer1881@gmail.com";
    $email_subject = "Contact Form via Urban Hygienist website";
 
    function died($error) 
	{
        // your error code can go here
		echo '<script>alert("We are very sorry, but there were error(s) found with the form you submitted.\n\nThese errors appear below.\n\n';
		echo $error;
        echo '\n\nPlease go back and fix these errors.\n\n")</script>';
        die();
    }
 
 
    // validation expected data exists
    if(!isset($_POST['name']) ||
		!isset($_POST['telephone'])) {
        died('We are sorry, but there appears to be a problem with the form you submitted.');       
    }
	
     
     
 
    $first_name = $_POST['name']; // required
	$telephone = $_POST['telephone']; //required
	$address = $_POST['address']; 
    $comments = $_POST['comments']; 
	$status = $_POST['status']; 
	$age = $_POST['age']; 
	$conditions = $_POST['conditions']; 
	$date = $_POST['date']; 
	
	//$error_message = '<script>alert("The name you entered does not appear to be valid.");</script>';
	//if($first_name == ""){
	//	echo $error_message;
	//}


 
    $string_exp = "/^[A-Za-z .'-]+$/";
 
  if(!preg_match($string_exp,$first_name)) {
    $error_message .= 'The name you entered does not appear to be valid.\n';
  }
 


 
  if(strlen($error_message) > 0) {
    died($error_message);
  }
 
    $email_message = "Form details below.\n\n";
 
     
    function clean_string($string) {
      $bad = array("content-type","bcc:","to:","cc:","href");
      return str_replace($bad,"",$string);
    }
 
    $email_message .= "Name: ".clean_string($first_name)."\n";
	$email_message .= "Telephone: ".clean_string($telephone)."\n"; 
	$email_message .= "Address: ".clean_string($address)."\n";  
    $email_message .= "Status: ".clean_string($status)."\n";  
    $email_message .= "Age: ".clean_string($age)."\n"; 
    $email_message .= "Conditions: ".clean_string($conditions)."\n";  
    $email_message .= "Other Medical Conditions : ".clean_string($comments)."\n";
    $email_message .= "Preferred Date for Appointment: ".clean_string($date)."\n"; 

// create email headers
$headers = 'From: '.'urbanhygienist@gmail.com'."\r\n".
'Reply-To: '.'urbanhygienist@gmail.com'."\r\n" .
'X-Mailer: PHP/' . phpversion();
@mail($email_to, $email_subject, $email_message, $headers);  
echo '<script>alert("谢谢!我们将会尽快联络您。")</script>';

?>
<!-- include your own success html here -->

<!--Thank you for contacting us. We will be in touch with you very soon.-->
<?php
 
}
?>

</body>
</html>