<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/CustomerDetails.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$comp = $_SESSION['company'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $id = $_POST["id"];
     $no_of_call = $_POST["no_of_call"];
     $update_no_of_call = $no_of_call + 1;
     $customer_name = $_POST["customer_name"];
     $customer_phoneno = $_POST["customer_phoneno"];    
     $tele_username = $_POST["tele_username"];
     $tele_uid = $_POST["tele_uid"];

     $recording = $_FILES['file']['name'];


     // $target_dir = "../uploads/";
     $target_dir = "../upload_recording/";
     $target_file = $target_dir . basename($_FILES["file"]["name"]);
     
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

     // Valid file extensions
     // $extensions_arr = array("jpg","jpeg","png","gif");
     $extensions_arr = array("jpg","jpeg","png","gif","mp3");

     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['file']['tmp_name'],$target_dir.$recording);
     }

     //for debugging
     echo "<br>";
     echo $id."<br>";
     echo $no_of_call."<br>";
     echo $update_no_of_call."<br>";
     echo $customer_name."<br>";
     echo $customer_phoneno."<br>";
     echo $tele_username."<br>";
     echo $tele_uid."<br>";
     echo $recording."<br>";

     if(isset($_POST['submit']))
     {
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          // //echo "save to database";

          if($update_no_of_call)
          {
               array_push($tableName,"no_of_call");
               array_push($tableValue,$update_no_of_call);
               $stringType .=  "s";
          }     

          if($recording)
          {
               array_push($tableName,"recording");
               array_push($tableValue,$recording);
               $stringType .=  "s";
          }  

          array_push($tableValue,$id);
          $stringType .=  "s";
          $updateCustomerDetails = updateDynamicData($conn,"customerdetails"," WHERE id = ? ",$tableName,$tableValue,$stringType);

          if($updateCustomerDetails)
          {
               // $_SESSION['messageType'] = 1;
               // header('Location: ../teleDashboard.php');
               
               $uid = $tele_uid;
               $teleName = $tele_username;
               $noOfuUpdate = $update_no_of_call;
               $customerName = $customer_name;
               $customerPhone = $customer_phoneno;
               $companyName = $comp;

               if (timeTeleUpdate($conn,$uid,$teleName,$customerName,$customerPhone,$noOfuUpdate,$companyName,$recording))
               {
                    echo "success";
                    //   echo "<script>alert('Data Updated and Stored !');window.location='../teleDashboard.php'</script>";   
               }
               else
               {
                    echo "fail"; 
               }
          }
          else
          {
               echo "unknown"; 
          }
     }
     else
     {
          echo "dunno";
     }
}
else
{
     header('Location: ../index.php');
}

function timeTeleUpdate($conn,$uid,$teleName,$customerName,$customerPhone,$noOfuUpdate,$companyName,$recording)
{
     if(insertDynamicData($conn,"time_teleupdate",array("uid","tele_name","customer_name","customer_phone","no_of_update","company_name","recording"),
     array($uid,$teleName,$customerName,$customerPhone,$noOfuUpdate,$companyName,$recording),"sssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

?>