<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Reason.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
// require_once dirname(__FILE__) . '/mailerFunction.php';


function addNewReason($conn,$status,$reason,$type)
{
     if(insertDynamicData($conn,"reason",array("status","reason","type"),
     array($status,$reason,$type),"sss") === null)
     {
          // echo "aaaa";
     }
     else
     {
          // echo "bbbb";
     }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $status = rewrite($_POST["add_status"]);
     $reason = rewrite($_POST["add_reason"]);
     $type = "1";

     // //   FOR DEBUGGING
     // echo "<br>";
     // echo $status."<br>";
     // echo $reason."<br>";
     // echo $type."<br>";

     if(addNewReason($conn,$status,$reason,$type))
     {
          header('Location: ../adminStatusReason.php?type=1');
     }
     else
     {
          header('Location: ../adminStatusReason.php?type=2');
     }
}
else
{
     header('Location: ../index.php');
}
?>