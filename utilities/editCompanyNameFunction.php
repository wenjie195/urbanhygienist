<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/CompanySelection.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $id = $_POST["company_id"];
     $companyName = $_POST["edit_company_name"];

     $companyDetails = getCompanySelection($conn," id = ?   ",array("id"),array($id),"i");    

     if(!$companyDetails)
     {   
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          //echo "save to database";
          if($companyName)
          {
               array_push($tableName,"company_name");
               array_push($tableValue,$companyName);
               $stringType .=  "s";
          }

          array_push($tableValue,$id);
          $stringType .=  "s";
          $updatedCompanyName = updateDynamicData($conn,"companyselection"," WHERE id = ? ",$tableName,$tableValue,$stringType);
          if($updatedCompanyName)
          {
               // echo "success";
               echo "<script>alert('successfully updated company name');window.location='../adminCompany.php'</script>";   
          }
          else
          {
               // echo "fail to update";
               echo "<script>alert('fail to update company name');window.location='../adminCompany.php'</script>";   
          }
     }
     else
     {
          // echo "GG";
          echo "<script>alert('ERROR !!');window.location='../adminCompany.php'</script>";   
     }
}
else 
{
     header('Location: ../index.php');
}
?>