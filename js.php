<div class="clear"></div>
<div class="uh-footer width100 same-padding white-text text-center margin-top0">
	© 2020 Urban Hygienist, All Rights Reserved.
</div>
<!-- <audio id="myAudio">
  <source src="css/noti2.ogg" type="audio/ogg">
  <source src="css/noti2.mp3" type="audio/mpeg">
 
</audio>
<div class="rrpowered_notification"></div> -->



<!-- Status Modal -->
<div id="status-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css status-content">
    <span class="close-css close-status close-status-css">&times;</span>
    <h2 class="green-text h2-title text-center">Add Status/Reason</h2>
    
    <!-- <form> -->
    <form action="utilities/addStatusReasonFunction.php" method="POST">
    	<p class="input-p">Status</p>
        <!-- <input class="input-style clean" type="text" placeholder="Status" id="add_status" name="add_status">    -->
        <select class="clean tele-input" id="add_status" name="add_status" required>
        <option value="">Select a Status</option>
          <?php 
          for ($cntPro=0; $cntPro <count($statusDetails) ; $cntPro++)
          {
          ?>
          <option value="<?php echo $statusDetails[$cntPro]->getStatus();?>"> 
          <?php echo $statusDetails[$cntPro]->getStatus(); ?>
          <!--take in display the options -->
          </option>
          <?php
          }
          ?>
        </select>

    	<p class="input-p">Reason</p>
        <input class="input-style clean" type="text" placeholder="Reason" id="add_reason" name="add_reason" required>           
        
        <div class="clear"></div>
        <div class="width100 text-center">
        	<button class="clean red-btn text-center modal-submit">Submit</button>
        </div>
    </form>
  </div>

</div>



<!-- Status Modal -->
<div id="company-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css status-content">
    <span class="close-css close-company close-status-css">&times;</span>
    <h2 class="green-text h2-title text-center">Add Company</h2>
    
    <!-- <form> -->
    <form action="utilities/addNewCompanyFunction.php" method="POST">
    	<p class="input-p">New Company Name</p>
        <input class="input-style clean" type="text" placeholder="Company Name" id="company_name" name="company_name" required>           
        
        <div class="clear"></div>
        <div class="width100 text-center">
        	<button class="clean red-btn text-center modal-submit">Submit</button>
        </div>
    </form>
  </div>

</div>

<!-- Contact Modal Box -->
<div id="modal-contact" class="modal">
  <div class="modal-content">
    <span class="close-contact close-css">&times;</span>
    			<h3 class="contact-h3 dark-text">Contact Us</h3>
                <form id="contactform" method="post" action="index.php" class="form-class extra-margin">
                  <p class="form-p dark-text">Name</p>
                  <input type="text" name="name" placeholder="Enter your name" class="input-name clean" required >
                  <p class="form-p dark-text">Email</p>
                  <input type="email" name="email" placeholder="Enter your email" class="input-name clean" required >
                  <p class="form-p dark-text">Phone</p>
                  <input type="text" name="telephone" placeholder="Enter your phone number" class="input-name clean" required >
                  <p class="form-p dark-text">Address</p>
                  <input type="text" name="address" placeholder="Enter your address" class="input-name clean" > 
                  <p class="form-p dark-text">Your Message</p>                 
                  <textarea name="comments" placeholder="Enter your message" class="input-message clean" ></textarea>
                  <div class="clear"></div>
                  <input type="radio" name="contact-option" value="contact-more-info" class="radio1 float-left clean" required><p class="opt-msg left"> I want to be contacted with more information about your company's offering marketing services and consulting</p>
                  <input type="radio" name="contact-option" value="contact-on-request" class="radio1 float-left clean"  required><p class="opt-msg left"> I just want to be contacted based on my request/inquiry</p>
                  <div class="clear"></div>
                   
                  <input type="submit" name="submit" value="SUBMIT" class="input-submit white-text clean pointer">
                </form> 
  </div>
</div>



<script src="js/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert-dev.js"></script>
<script src="js/notification.js"></script>
<script src="js/notiflix-aio-1.9.1.js"></script>
<script src="js/notiflix-aio-1.9.1.min.js"></script>
<script src="js/rrpowered_notification_script.js"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>  
<script src="js/jquery.fancybox.js" type="text/javascript"></script>    
<script src="js/headroom.js"></script>
<script>
    $(window).load(function(){
       // PAGE IS FULLY LOADED  
       // FADE OUT YOUR OVERLAYING DIV
       $('#overlay').fadeOut();
    });
</script>
<script>
    ;(function(){
      function id(v){return document.getElementById(v); }
      function loadbar() {
        var ovrl = id("overlay"),
            prog = id("progress"),
            stat = id("progstat"),
            img = document.images,
            c = 0;
            tot = img.length;
    
        function imgLoaded(){
          c += 1;
          var perc = ((100/tot*c) << 0) +"%";
          prog.style.width = perc;
          stat.innerHTML = "Loading "+ perc;
          if(c===tot) return doneLoading();
        }
        function doneLoading(){
          ovrl.style.opacity = 0;
          setTimeout(function(){ 
            ovrl.style.display = "none";
          }, 1200);
        }
        for(var i=0; i<tot; i++) {
          var tImg     = new Image();
          tImg.onload  = imgLoaded;
          tImg.onerror = imgLoaded;
          tImg.src     = img[i].src;
        }    
      }
      document.addEventListener('DOMContentLoaded', loadbar, false);
    }());
</script>
<script>
    (function() {
        var header = new Headroom(document.querySelector("#header"), {
            tolerance: 5,
            offset : 205,
            classes: {
              initial: "animated",
              pinned: "slideDown",
              unpinned: "slideUp"
            }
        });
        header.init();
    
    }());
</script>
 
	<script>
    // Cache selectors
    var lastId,
        topMenu = $("#top-menu"),
        topMenuHeight = topMenu.outerHeight(),
        // All list items
        menuItems = topMenu.find("a"),
        // Anchors corresponding to menu items
        scrollItems = menuItems.map(function(){
          var item = $($(this).attr("href"));
          if (item.length) { return item; }
        });
    
    // Bind click handler to menu items
    // so we can get a fancy scroll animation
    menuItems.click(function(e){
      var href = $(this).attr("href"),
          offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
      $('html, body').stop().animate({ 
          scrollTop: offsetTop
      }, 500);
      e.preventDefault();
    });
    
    // Bind to scroll
    $(window).scroll(function(){
       // Get container scroll position
       var fromTop = $(this).scrollTop()+topMenuHeight;
       
       // Get id of current scroll item
       var cur = scrollItems.map(function(){
         if ($(this).offset().top < fromTop)
           return this;
       });
       // Get the id of the current element
       cur = cur[cur.length-1];
       var id = cur && cur.length ? cur[0].id : "";
                      
    });
    </script>
	<script>
		$(document).ready(function() {
		var s = $(".menu-white");
		var pos = s.position();					   
		$(window).scroll(function() {
			var windowpos = $(window).scrollTop();
			if (windowpos >= pos.top & windowpos >=200) {
				s.addClass("menu-green");
			} else {
				s.removeClass("menu-green");	
			}
			});
		});
		if (window.matchMedia('screen and (max-width: 1500px)').matches) {
		$(document).ready(function() {
		var s = $(".menu-white");
		var pos = s.position();					   
		$(window).scroll(function() {
			var windowpos = $(window).scrollTop();
			if (windowpos >= pos.top & windowpos >=100) {
				s.addClass("menu-green");
			} else {
				s.removeClass("menu-green");	
			}
			});
		});		
			}
	</script> 

<script type="text/javascript" src="js/modernizr.custom.js"></script>
<script type="text/javascript" src="js/jquery.dlmenu.js"></script>
		<script>
			$(function() {
				$( '#dl-menu' ).dlmenu({
					animationClasses : { classin : 'dl-animate-in-2', classout : 'dl-animate-out-2' }
				});
			});
    </script>
<!--- Modal Box --->
<script>
var modalContact = document.getElementById("modal-contact");
var statusmodal = document.getElementById("status-modal");

var companymodal = document.getElementById("company-modal");

var btnContact1 = document.getElementsByClassName("open-contact")[0];
var btnContact2 = document.getElementsByClassName("open-contact")[1];	
var openstatus = document.getElementsByClassName("open-status")[0];

var spanContact = document.getElementsByClassName("close-contact")[0];	
var closestatus = document.getElementsByClassName("close-status")[0];

var opencompany = document.getElementsByClassName("open-company")[0];

var closecompany = document.getElementsByClassName("close-company")[0];

if(btnContact1){
btnContact1.onclick = function() {
	  modalContact.style.display = "block";
}
}
if(btnContact2){
	btnContact2.onclick = function() {
	  modalContact.style.display = "block";
	}
}
if(openstatus){
openstatus.onclick = function() {
  statusmodal.style.display = "block";
}
}

if(spanContact){
	spanContact.onclick = function() {
	  modalContact.style.display = "none";
	}
}
if(closestatus){
closestatus.onclick = function() {
  statusmodal.style.display = "none";
}
}

if(opencompany){
  opencompany.onclick = function() {
  companymodal.style.display = "block";
}
}

if(closecompany){
  closecompany.onclick = function() {
  companymodal.style.display = "none";
}
}

window.onclick = function(event) {
  if (event.target == statusmodal) {
    statusmodal.style.display = "none";
  }       
  if (event.target == modal) {
		modalContact.style.display = "none";
	  }
  if (event.target == companymodal) {
    companymodal.style.display = "none";
  }       
}
</script>