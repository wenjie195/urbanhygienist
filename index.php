<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://urbanhygienist.com/" />
<meta property="og:title" content="Allergy Cleaning Services - Dust Mites Removal | Urban Hygienist" />
<title>Allergy Cleaning Services - Dust Mites Removal | Urban Hygienist</title>

<link rel="canonical" href="https://urbanhygienist.com/" />
<?php include 'css.php'; ?>
</head>

<body class="body">

<div id="overlay">
 <div class="center-food"><img src="img/loading-gif.gif" class="food-gif" alt="Allergy Cleaning Services" title="Allergy Cleaning Services"></div>
 <div id="progstat"></div>
 <div id="progress"></div>
</div>

    <div class="landing-first-div width100 same-padding">
        <!-- Start Menu -->
        <header id="header" class="header header--fixed same-padding header1 menu-white index-menu" role="banner">
            <div class="big-container-size hidden-padding">
                <div class="left-logo-div float-left hidden-logo-padding">
                    <img src="img/urban-hygienist.png" class="logo-img" alt="Urban Hygienist" title="Urban Hygienist">
                </div>
                
                <div class="right-menu-div float-right" id="top-menu">
                    <a href="#about" class="green-text-a menu-padding">About</a>
                    <a href="#services" class="green-text-a menu-padding">Services</a>
                    <a href="#whyus" class="green-text-a menu-padding">Why Us</a>
                    <a href="#testi" class="green-text-a menu-padding">Testimonials</a>
                    <a href="#contact" class="green-text-a menu-padding">Contact Us</a>
                    <a href="cn.php" class="green-text-a menu-padding" style="font-family:kaiti;">中文</a>
                    <a href="login.php" class="green-text-a menu-padding">Login</a>
                    <a class="open-menu menu-icon"><img src="img/menu.png" class="menu-img"></a>
                <!-- Mobile View-->
                <!--    <a href="#whatyouneed" class="white-text menu-padding red-hover2">
                        <img src="img/thousand-media/menu-icon-14.png" class="menu-img" alt="Your Need" title="Your Need">
                    </a>
                    <a href="#promotion" class="white-text menu-padding red-hover2">
                        <img src="img/thousand-media/menu-icon-13.png" class="menu-img" alt="Promotion" title="Promotion">
                    </a>
        
                    <a href="#services" class="white-text menu-padding red-hover2">
                        <img src="img/thousand-media/menu-icon-11.png" class="menu-img" alt="Services" title="Services">            
                    </a>
                    <a href="#contact" class="white-text menu-padding red-hover2">
                        <img src="img/thousand-media/menu-icon-10.png" class="menu-img" alt="Contact" title="Contact">            
                    </a>
                    <a class="white-text menu-padding red-hover2 open-register pointer">
                        <img src="img/thousand-media/menu-icon-15.png" class="menu-img" alt="Register" title="Register">            
                    </a>
                    <a class="white-text red-hover2 open-login pointer">
                        <img src="img/thousand-media/menu-icon-16.png" class="menu-img" alt="Login" title="Login">            
                    </a>        -->    
                </div>
            </div>
        
        </header>
        <h1 class="dark-text h1-title first-h1">Protect your family from dust mites and allergens today.</h1>
        <p class="h1-p dark-text2">Let us show you how.</p>
        <a class="open-contact"><div class="white-button hover-white open-contact first-white-div">Get Your Free<br>Mattress Cleaning Service Now</div></a>
    
    
    </div>
<div id="about">    
    <div class="carousel-div">
        <section class="single-item">
            <div>
            	<div class="img-div">
              		<img src="img/vivenso.png" class="carousel-img" alt="Vivenso - Multifunction Cleaning System" title="Vivenso - Multifunction Cleaning System">
                    <p class="product-p text-center">Vivenso - Multifunction Cleaning System</p>
            	</div>
            </div>
            <div>
            	<div class="img-div">
            		<img src="img/airwasher.png" class="carousel-img" alt="Venta - Air Washer" title="Venta - Air Washer">
                    <p class="product-p text-center">Venta - Air Washer</p>
                </div>
            </div>
            <div>
            	<div class="img-div">
            		<img src="img/air-freshener.png" class="carousel-img" alt="Airvenue - Air Freshener" title="Airvenue - Air Freshener">
                    <p class="product-p text-center">Airvenue - Air Freshener</p>
                </div>
            </div>            
        </section>    
    </div>
    <div class="clear"></div>
    <div class="width100 same-padding cleanliness text-center margin-auto">
    	<p class="sub-title dark-text">Cleanliness for those who care.</p>
        <p class="normal-content dark-text margin-auto">Urban Hygienist provides you with an effective cleaning solution to control eczema, allergic rhinitis, and asthma, by removing dust, dust mites and allergens in your home with eco-friendly water filter technology.</p>
    </div>
    <div class="animated-div same-padding margin-auto">
    	<img src="img/dining.gif" class="animated-img" alt="Allergy-free Environment" title="Allergy-free Environment">
    	<p class="animated-p text-center">Enjoy quality family time in an allergy-free environment.</p>
    </div>
    <div class="animated-div same-padding margin-auto">
    	<img src="img/sleep2.gif" class="animated-img" alt="No more breathing problems caused by allergies and asthma" title="No more breathing problems caused by allergies and asthma">
    	<p class="animated-p text-center">No more breathing problems caused by allergies and asthma while you sleep.</p>
    </div>    
    <div class="animated-div same-padding margin-auto">
    	<img src="img/living3.gif" class="animated-img" alt="Healthy Home" title="Healthy Home">
    	<p class="animated-p text-center">Spend more time with your loved ones with less cleaning to do in a healthy home.</p>
    </div>    

    <div class="animated-div same-padding margin-auto">
    	<iframe class="animated-img iframe-video" src="https://www.youtube.com/embed/b9C9mBYh0ig" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div> 

    
</div>
<div class="grey-bg">
    <div id="services">
        <div class="clear"></div>
        <div class="width100 same-padding distance">
            <div class="width100 text-center">
                <img src="img/our-service.png" class="text-center three-img--top service-img" alt="Urban Hygienist Services" title="Urban Hygienist Services">
                <p class="sub-title dark-text text-center">Our Services</p>
            </div>
            <div class="clear"></div>
            <div class="three-div text-center hover-a open-mattress">
                <img src="img/mattress-cleaning.png" class="three-img" alt="Mattress Cleaning" title="Mattress Cleaning">
                <p class="desc-p dark-text text-center">Mattress Cleaning</p>
            </div>
            <div class="three-div text-center middle-three second-three hover-a open-air">
                <img src="img/room-air-cleaning.png" class="three-img" alt="Air Treatment" title="Air Treatment">
                <p class="desc-p dark-text text-center">Air Treatment</p>
            </div>
            <div class="three-div text-center hover-a open-curtain">
                <img src="img/curtain-cleaning.png" class="three-img" alt="Curtain Cleaning" title="Curtain Cleaning">
                <p class="desc-p dark-text text-center">Curtain Cleaning</p>
            </div>    
            <div class="three-div text-center second-three hover-a open-carpet">
                <img src="img/carpet-cleaning.png" class="three-img" alt="Floor and Carpet Cleaning" title="Floor and Carpet Cleaning">
                <p class="desc-p dark-text text-center">Floor and Carpet Cleaning</p>
            </div> 
            <div class="three-div text-center middle-three hover-a open-sofa">
                <img src="img/sofa-cleaning.png" class="three-img" alt="Sofa Cleaning" title="Sofa Cleaning">
                <p class="desc-p dark-text text-center">Sofa Cleaning</p>
            </div>        
            <div class="three-div text-center second-three hover-a open-floor">
                <img src="img/maid-service.png" class="three-img" alt="Maid Service" title="Maid Service">
                <p class="desc-p dark-text text-center">Maid Service</p>
            </div>                    
        </div>
    </div>

    
        <div class="clear"></div>
        <div class="width100 same-padding distance2">
        <div id="whyus">
            <div class="width100 text-center">
                <img src="img/why-us.png" class="text-center three-img--top" alt="Why Urban Hygienist" title="Why Urban Hygienist">
                <p class="sub-title dark-text text-center">Why Us</p>
            </div>
            <div class="three-div2">
             	<a href="./img/plus-x-award.png" data-fancybox title="plus x award">
                	<img src="img/award-winning-german-technology.png" class="width100 opacity-hover" alt="Award Winning German Technology" title="Award Winning German Technology">
                </a>           
            </div>
            <div class="three-div2 middle-three-2">
            	<a href="./img/cert1.png" 
                     data-fancybox="images-preview"  title="Quality Seal for Allergy-Friendly Products and Services"
                     >
                	<img src="img/certified-preventative-medical-product.png" class="width100 opacity-hover" alt="Certified Preventative Medical Product For Allergic Persons" title="Certified Preventative Medical Product For Allergic Persons">
                </a>
                <div style="display: none;">
                 
                
                  <a href="./img/cert2.png" data-fancybox="images-preview" title="Produktsiegel Certificate Allergiker-geeignet" ></a>
                
                  <a href="./img/cert3.png" data-fancybox="images-preview" title="Produktsiegel Certificate Allergiker-geeignet" ></a>
                
                  <a href="./img/cert4.png" data-fancybox="images-preview" title="Health Product - Room Cleaning System"></a>
                  <a href="./img/cert5.png" data-fancybox="images-preview" title="Health Product - Air Refresher"></a>
                </div>            
            
       		</div>
            </div>
            <div class="three-div2">
                <img src="img/10-year-warranty.png" class="width100" alt="Up to 10 years warranty* " title="Up to 10 years warranty* ">
            </div>                   
        </div>

        <div class="clear"></div>
       <div id="testi">
        <div class="width100 same-padding distance2 urban-bg">
            <div class="width100 text-center">
                <img src="img/review2.png" class="text-center three-img--top" alt="Why Urban Hygienist" title="Why Urban Hygienist">
                <p class="sub-title dark-text text-center test-p">Testimonials</p>
                <p class="product-p text-center">See how our customers feel about us</p>
                <iframe class="review-iframe iframe1"  src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Furbanhygienist%2F&tabs&width=500&height=214&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=2101026363514817" width="500" height="214" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                <div class="clear"></div>
                <iframe class="review-iframe iframe1a" src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fkae.chyi.3%2Fposts%2F10157337714606907&width=500" width="500" height="199" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                <div class="clear"></div>
                <iframe class="review-iframe iframe1b" src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Flynnkhoo1011%2Fposts%2F10155863386462810&width=500" width="500" height="199" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                <div class="clear"></div>
                <iframe class="review-iframe iframe1c" src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fchuaseohli%2Fposts%2F10159140402273647&width=500" width="500" height="180" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                <div class="clear"></div>              
                <iframe class="review-iframe iframe1d"  src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fchiew.anna%2Fposts%2F2199735336724301&width=500" width="500" height="199" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                <div class="clear"></div>
                <iframe class="review-iframe iframe1e" src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fngai.hung.50%2Fposts%2F10155904515418565&width=500" width="500" height="161" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                <div class="clear"></div>
				<iframe class="review-iframe iframe2aa" src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fsky.lc.5%2Fposts%2F2089956524419845&width=500" width="500" height="661" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                <div class="clear"></div>
                <iframe class="review-iframe iframe3" src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fmelinda.ho.121%2Fposts%2F10156317603128870&width=500" width="500" height="661" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                <div class="clear"></div>
            </div>        
      </div>
    </div>
        
        <div class="clear"></div>
        <div class="last-div width100 same-padding distance text-center">
        	<div id="contact">
                <h1 class="h1-title last-h1 text-center">Enhance your home for your family now.</h1>
                <p class="text-center"><a class="open-contact hover-white"><div class="white-button margin-auto open-contact contact-white-button">Get Your Free<br>Mattress Cleaning Service Now</div></a></p>
 				<p class="text-center social-media-p">
                	<a class="opacity-hover" target="_blank" href="https://www.facebook.com/urbanhygienist/"><img src="img/facebook.png" class="social-icon social-icon1" alt="Urban Hygienist Facebook" title="Urban Hygienist Facebook"></a>
                	<a class="opacity-hover" target="_blank" href="https://www.instagram.com/myurbanhygienist/"><img src="img/instagram.png" class="social-icon social-icon2" alt="Urban Hygienist Instagram" title="Urban Hygienist Instagram"></a>
                </p>
            </div>
            
        </div>
   

</div>

<div class="clear"></div>
<div class="uh-footer width100 same-padding white-text text-center">
	© 2020 Urban Hygienist, All Rights Reserved.
</div>

<!-- Contact Modal Box -->
<div id="modal-contact" class="modal">
  <div class="modal-content">
    <span class="close-contact close-css">&times;</span>
    			<h2 class="contact-h2 dark-text">Fill In To Redeem Free Voucher</h2>
                <form id="contactform" method="post" action="index.php" class="form-class extra-margin">
                  <p class="form-p dark-text">Name</p>
                  <input type="text" name="name" placeholder="Enter your name" class="input-name clean" required >
                  <p class="form-p dark-text">Contact Number</p>
                  <input type="text" name="telephone" placeholder="Enter your phone number" class="input-name clean" required >


                  <p class="form-p dark-text">Address</p>
                  <input type="text" name="address" placeholder="Enter your address" class="input-name clean" >
                  <p class="form-p dark-text">Status</p> 
                  <select class="clean input-name input-select" name="status">
                  	<option class="clean input-name input-option">-</option>
                  	<option class="clean input-name input-option">Not Married</option>
                    <option class="clean input-name input-option">Married</option>
                  </select>
                  <p class="form-p dark-text">Age</p> 
                  <select class="clean input-name input-select" name="age">
                  <option class="clean input-name input-option">-</option>
                  	<option class="clean input-name input-option">20-29</option>
                    <option class="clean input-name input-option">30-40</option>
                    <option class="clean input-name input-option">41-50</option>
                    <option class="clean input-name input-option">51-60</option>
                    <option class="clean input-name input-option">61-70</option>
                    <option class="clean input-name input-option">> 70</option>
                    <option class="clean input-name input-option">< 20</option>
                  </select>
                  <p class="form-p dark-text">Do you or your family have any of the medical conditions below?</p> 
                  <select class="clean input-name input-select" name="conditions">
                  	<option class="clean input-name input-option">-</option>
                  	<option class="clean input-name input-option">None</option>
                  	<option class="clean input-name input-option">Asthma</option>
                    <option class="clean input-name input-option">Sinus</option>
                    <option class="clean input-name input-option">Skin sensitivity</option>
                    <option class="clean input-name input-option">Others</option>
                  </select> 
                  <p class="form-p dark-text">If you choose others, please specify:</p> 
                  <textarea placeholder="Other Medical Conditions" class="input-name input-message clean" name="comments"  ></textarea>                                   
                  <p class="form-p dark-text">Preferred Date for Appointment</p> 
                  <select class="clean input-name input-select" name="date">
                  	<option class="clean input-name input-option">-</option>
                  	<option class="clean input-name input-option">Weekday</option>
                    <option class="clean input-name input-option">Weekend</option>
                  </select>                    
                  <div class="clear"></div>

                 
                  <div class="res-div"><input type="submit" name="submit" value="Submit and Get My Voucher" class="input-submit white-text clean pointer" onClick="myFunction5()"></div>
                </form> 
  </div>
</div>

<!-- Menu Modal Box -->
<div id="modal-menu" class="modal menu-modal-css">
  <div class="modal-content menu-modal-content">
    <span class="close-menu close-css">&times;</span>
		<h2 class="menu-h2 text-center dark-text">Menu</h2>
        <p class="menu-p text-center"><a href="#about" class="menu-p-a green-text-a close-menu">About</a></p>
        <p class="menu-p text-center"><a href="#services" class="menu-p-a green-text-a close-menu">Services</a></p>
        <p class="menu-p text-center"><a href="#whyus" class="menu-p-a green-text-a close-menu">Why Us</a></p>
        <p class="menu-p text-center"><a href="#testi" class="menu-p-a green-text-a close-menu">Testimonials</a></p> 
        <p class="menu-p text-center"><a href="#contact" class="menu-p-a green-text-a close-menu">Contact Us</a></p> 
        <p class="menu-p text-center"><a href="cn.php" class="menu-p-a green-text-a" style="font-family:kaiti;">中文</a></p> 
        <p class="menu-p text-center"><a href="login.php" class="menu-p-a green-text-a close-menu">Login</a></p>                              				
  </div>
</div>


<!-- Mattress Modal Box -->
<div id="modal-mattress" class="modal mattress-modal-css">
  <span class="close-mattress close-mattress2 close-css">&times;</span>
  <span class="arrow-span left-arrow open-floor"><img src="img/prev.png" class="arrow-img"></span>
  <span class="arrow-span right-arrow open-air"><img src="img/next.png" class="arrow-img"></span>
  <div class="modal-content mattress-modal-content">
    	
		<div class="modal-left-div mattress-bg"></div>
        <div class="modal-right-div">
        	<h2 class="service-title">Mattress Cleaning</h2>
            <ul class="service-ul">
            	<li class="service-li">Our beds and pillows are one of the favorite hangout spots for dust mites and bacteria. While we are asleep, dust mites come out for picnics to feast on our dead skin right off of our skin. The thing causing itchiness and allergy reactions are not actually the pests themselves, but the droppings they leave.</li>
                 <li class="service-li">We spend more than one-third of our lives sleeping at night. Could you imagine all the picnic sessions the dust mites have had while you are not deep cleaning your bed?</li>
                <li class="service-li">It is best to have your mattresses cleaned at least every 3 months so that you and your family can enjoy a healthy and good night’s sleep.</li>
            </ul>
        </div>                     				
  </div>
</div>


<!-- Room Air Cleaning Modal Box -->
<div id="modal-air" class="modal mattress-modal-css">
  <span class="close-air close-mattress2 close-css">&times;</span>
  <span class="arrow-span left-arrow open-mattress"><img src="img/prev.png" class="arrow-img"></span>
  <span class="arrow-span right-arrow open-curtain"><img src="img/next.png" class="arrow-img"></span>
  <div class="modal-content mattress-modal-content">
    	
		<div class="modal-left-div room-bg"></div>
        <div class="modal-right-div">
        	<h2 class="service-title">Air Treatment</h2>
            <ul class="service-ul">
            	<li class="service-li">While it’s easy to see when you need to sweep the floor or dust the shelves, it’s harder to know when the air in your home needs cleaning.</li>
                <li class="service-li">Did you know that the accumulation of bacteria and dust from our beds, curtains, sofas, and carpet makes indoor air 5 times more polluted than outdoor air pollution? As we spend 90% of our lives indoors, most of the air we breathe is indoors which could harm our health in the long run.</li>
                <li class="service-li">While we often keep our windows and doors closed to prevent outdoor particles from coming in; we are also preventing the indoor toxins from getting out. But that’s just something Urban Hygienist can easily solve!</li>
            </ul>
        </div>                     				
  </div>
</div>


<!-- Curtain Cleaning Modal Box -->
<div id="modal-curtain" class="modal mattress-modal-css">
  <span class="close-curtain close-mattress2 close-css">&times;</span>
  <span class="arrow-span left-arrow open-air"><img src="img/prev.png" class="arrow-img"></span>
  <span class="arrow-span right-arrow open-carpet"><img src="img/next.png" class="arrow-img"></span>
  <div class="modal-content mattress-modal-content">
    	
		<div class="modal-left-div curtain-bg"></div>
        <div class="modal-right-div">
        	<h2 class="service-title">Curtain Cleaning</h2>
            <ul class="service-ul">
            	<li class="service-li">Most people tend to forget about curtains while cleaning their homes. Little did they know, curtains can cause serious harm to your health if left unattended for long periods of time. </li>
                <li class="service-li">Do you notice the tiny specks of flying bits whenever you open up your curtains? Those are what causes allergy reactions and trigger asthmatic problems. Due to their small size, they are easily carried by wind and will accumulate on curtains after some time.</li>
                <li class="service-li">Curtains serve as shields for our home as it withstands heat, cold, direct sunlight and dirt. So, make sure your home’s first line of defense is allergen-free with the help of Urban Hygienist.</li>
            </ul>
        </div>                     				
  </div>
</div>


<!-- Carpet Cleaning Modal Box -->
<div id="modal-carpet" class="modal mattress-modal-css">
  <span class="close-carpet close-mattress2 close-css">&times;</span>
  <span class="arrow-span left-arrow open-curtain"><img src="img/prev.png" class="arrow-img"></span>
  <span class="arrow-span right-arrow open-sofa"><img src="img/next.png" class="arrow-img"></span>
  <div class="modal-content mattress-modal-content">
    	
		<div class="modal-left-div carpet-bg"></div>
        <div class="modal-right-div">
        	<h2 class="service-title">Floor and Carpet Cleaning</h2>
            <ul class="service-ul">
            	<li class="service-li">We are not trying to be dramatic, but did you know the fact that an average indoor carpet is about 4,000 times dirtier than a toilet seat, with about 200,000 bacteria per square inch?</li>
                <li class="service-li">When outdoor debris and bacteria fall into carpet fibers, they make a home out of it, turning your carpet into a hotbed of bacteria, dust, pollen, and dead skin particles. These allergens are the cause of respiratory issues and wheezing when they get kicked into the air.</li>
                <li class="service-li">Our well-trained service professionals at Urban Hygienist will ensure your family will not be threatened by these horrible allergens once and for all.</li>
            </ul>
        </div>                     				
  </div>
</div>

<!-- Sofa Cleaning Modal Box -->
<div id="modal-sofa" class="modal mattress-modal-css">
  <span class="close-sofa close-mattress2 close-css">&times;</span>
  <span class="arrow-span left-arrow open-carpet"><img src="img/prev.png" class="arrow-img"></span>
  <span class="arrow-span right-arrow open-floor"><img src="img/next.png" class="arrow-img"></span>
  <div class="modal-content mattress-modal-content">
    	
		<div class="modal-left-div sofa-bg"></div>
        <div class="modal-right-div">
        	<h2 class="service-title">Sofa Cleaning</h2>
            <ul class="service-ul">
            	<li class="service-li">After a hard day at work, the sofa is where we plop down on as soon as we get home. Usually, we don’t bother to change out of our work attire into our house clothes first because our favorite TV program is already starting, and there is no shame in doing that.</li>
                <li class="service-li">While we are enjoying, the outdoor bacteria and allergens on your work attire are moving into the sofa, and into your home.</li>
                <li class="service-li">Keep your sofa clean, looking fresh and rid of microscopic contaminants that can only be removed with thorough and professional deep cleaning.</li>
            </ul>
        </div>                     				
  </div>
</div>

<!-- Maid Modal Box -->
<div id="modal-floor" class="modal mattress-modal-css">
  <span class="close-floor close-mattress2 close-css">&times;</span>
  <span class="arrow-span left-arrow open-sofa"><img src="img/prev.png" class="arrow-img"></span>
  <span class="arrow-span right-arrow open-mattress"><img src="img/next.png" class="arrow-img"></span>
  <div class="modal-content mattress-modal-content">
    	
		<div class="modal-left-div maid-bg"></div>
        <div class="modal-right-div">
        	<h2 class="service-title">Maid Service</h2>
            <ul class="service-ul">
            	<li class="service-li">If you prefer to kick back and relax while someone else does the cleaning for you, we’ve got your back! Our team of professionally trained cleaners will ensure your home is thoroughly cleaned once they finish their work.</li>
            </ul>
        </div>                     				
  </div>
</div>
<style>
@media all and (max-width: 550px){
.menu-padding {
    margin-left: 12px;
	font-size: 13px !important;}
	
}
</style>
<script src="js/jquery-3.3.1.min.js" type="text/javascript"></script> 
<script src="js/bootstrap.min.js" type="text/javascript"></script>  
<script src="js/jquery.fancybox.js" type="text/javascript"></script>  
<script src="slick/slick.js" type="text/javascript" charset="utf-8"></script>  
<script>
	$('.single-item').slick();
</script>
<script src="js/headroom.js"></script>
<script>
    $(window).load(function(){
       // PAGE IS FULLY LOADED  
       // FADE OUT YOUR OVERLAYING DIV
       $('#overlay').fadeOut();
    });
</script>
<script>
    ;(function(){
      function id(v){return document.getElementById(v); }
      function loadbar() {
        var ovrl = id("overlay"),
            prog = id("progress"),
            stat = id("progstat"),
            img = document.images,
            c = 0;
            tot = img.length;
    
        function imgLoaded(){
          c += 1;
          var perc = ((100/tot*c) << 0) +"%";
          prog.style.width = perc;
          stat.innerHTML = "Loading "+ perc;
          if(c===tot) return doneLoading();
        }
        function doneLoading(){
          ovrl.style.opacity = 0;
          setTimeout(function(){ 
            ovrl.style.display = "none";
          }, 1200);
        }
        for(var i=0; i<tot; i++) {
          var tImg     = new Image();
          tImg.onload  = imgLoaded;
          tImg.onerror = imgLoaded;
          tImg.src     = img[i].src;
        }    
      }
      document.addEventListener('DOMContentLoaded', loadbar, false);
    }());
</script>
<script>
    (function() {
        var header = new Headroom(document.querySelector("#header"), {
            tolerance: 5,
            offset : 205,
            classes: {
              initial: "animated",
              pinned: "slideDown",
              unpinned: "slideUp"
            }
        });
        header.init();
    
    }());
</script>
 
	<script>
    // Cache selectors
    var lastId,
        topMenu = $("#top-menu"),
        topMenuHeight = topMenu.outerHeight(),
        // All list items
        menuItems = topMenu.find("a"),
        // Anchors corresponding to menu items
        scrollItems = menuItems.map(function(){
          var item = $($(this).attr("href"));
          if (item.length) { return item; }
        });
    
    // Bind click handler to menu items
    // so we can get a fancy scroll animation
    menuItems.click(function(e){
      var href = $(this).attr("href"),
          offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
      $('html, body').stop().animate({ 
          scrollTop: offsetTop
      }, 500);
      e.preventDefault();
    });
    
    // Bind to scroll
    $(window).scroll(function(){
       // Get container scroll position
       var fromTop = $(this).scrollTop()+topMenuHeight;
       
       // Get id of current scroll item
       var cur = scrollItems.map(function(){
         if ($(this).offset().top < fromTop)
           return this;
       });
       // Get the id of the current element
       cur = cur[cur.length-1];
       var id = cur && cur.length ? cur[0].id : "";
                      
    });
    </script>
	
    
<script>
	//Contact
	var modalContact = document.getElementById("modal-contact");
	

	var btnContact1 = document.getElementsByClassName("open-contact")[0];
	var btnContact2 = document.getElementsByClassName("open-contact")[1];	
	var btnContact3 = document.getElementsByClassName("open-contact")[2];
	var btnContact4 = document.getElementsByClassName("open-contact")[3];	

	var spanContact = document.getElementsByClassName("close-contact")[0];

	//Menu	
	var modalMenu = document.getElementById("modal-menu");
	

	var btnMenu = document.getElementsByClassName("open-menu")[0];

	var spanMenu1 = document.getElementsByClassName("close-menu")[0];
	var spanMenu2 = document.getElementsByClassName("close-menu")[1];
	var spanMenu3 = document.getElementsByClassName("close-menu")[2];
	var spanMenu4 = document.getElementsByClassName("close-menu")[3];
	var spanMenu5 = document.getElementsByClassName("close-menu")[4];
	var spanMenu6 = document.getElementsByClassName("close-menu")[5];			


	//Mattress	
	var modalMattress = document.getElementById("modal-mattress");
	

	var btnMattress = document.getElementsByClassName("open-mattress")[0];
	var btnMattress2 = document.getElementsByClassName("open-mattress")[1]; //Room Air Cleaing arrow
	var btnMattress3 = document.getElementsByClassName("open-mattress")[2];
	
	var spanMattress = document.getElementsByClassName("close-mattress")[0];


	//Room Air Cleaning	
	var modalAir = document.getElementById("modal-air");
	

	var btnAir = document.getElementsByClassName("open-air")[0]; 
	var btnAir2 = document.getElementsByClassName("open-air")[1]; //Mattress Arrow
	var btnAir3 = document.getElementsByClassName("open-air")[2]; //Curtain Arrow
	
	var spanAir = document.getElementsByClassName("close-air")[0];	
	


	//Curtain Cleaning	
	var modalCurtain = document.getElementById("modal-curtain");
	

	var btnCurtain = document.getElementsByClassName("open-curtain")[0]; 
	var btnCurtain2 = document.getElementsByClassName("open-curtain")[1]; //Air Arrow
	var btnCurtain3 = document.getElementsByClassName("open-curtain")[2]; //Carpet Arrow
	
	var spanCurtain = document.getElementsByClassName("close-curtain")[0];		


	//Carpet Cleaning	
	var modalCarpet = document.getElementById("modal-carpet");
	

	var btnCarpet = document.getElementsByClassName("open-carpet")[0]; 
	var btnCarpet2 = document.getElementsByClassName("open-carpet")[1]; 
	var btnCarpet3 = document.getElementsByClassName("open-carpet")[2]; 
	
	var spanCarpet = document.getElementsByClassName("close-carpet")[0];

	//Sofa Cleaning	
	var modalSofa = document.getElementById("modal-sofa");
	

	var btnSofa = document.getElementsByClassName("open-sofa")[0]; 
	var btnSofa2 = document.getElementsByClassName("open-sofa")[1]; 
	var btnSofa3 = document.getElementsByClassName("open-sofa")[2]; 
	
	var spanSofa = document.getElementsByClassName("close-sofa")[0];		

	//Maid Service	
	var modalFloor = document.getElementById("modal-floor");
	

	var btnFloor = document.getElementsByClassName("open-floor")[0]; 
	var btnFloor2 = document.getElementsByClassName("open-floor")[1]; 
	var btnFloor3 = document.getElementsByClassName("open-floor")[2]; 
	
	var spanFloor = document.getElementsByClassName("close-floor")[0];	

	

	btnContact1.onclick = function() {
	  modalContact.style.display = "block";
	}
	btnContact2.onclick = function() {
	  modalContact.style.display = "block";
	}	
	btnContact3.onclick = function() {
	  modalContact.style.display = "block";
	}
	btnContact4.onclick = function() {
	  modalContact.style.display = "block";
	}
	btnMenu.onclick = function() {
	  modalMenu.style.display = "block";
	}
	btnMattress.onclick = function() {
	  modalMattress.style.display = "block";
	}
	btnMattress2.onclick = function() {
	  modalAir.style.display = "none";
	  modalMattress.style.display = "block";
	}
	btnMattress3.onclick = function() {
	  modalFloor.style.display = "none";
	  modalMattress.style.display = "block";
	}	
	btnAir.onclick = function() {
	  modalAir.style.display = "block";	  
	}	
	btnAir2.onclick = function() {
	  modalMattress.style.display = "none";
	  modalAir.style.display = "block";	  
	}
	btnAir3.onclick = function() {
	  modalCurtain.style.display = "none";
	  modalAir.style.display = "block";	  
	}
	btnCurtain.onclick = function() {
	  modalCurtain.style.display = "block";	  
	}	
	btnCurtain2.onclick = function() {
	  modalAir.style.display = "none";
	  modalCurtain.style.display = "block";	  
	}
	btnCurtain3.onclick = function() {
	  modalCarpet.style.display = "none";
	  modalCurtain.style.display = "block";	  
	}
	btnCarpet.onclick = function() {
	  modalCarpet.style.display = "block";	  
	}	
	btnCarpet2.onclick = function() {
	  modalCurtain.style.display = "none";
	  modalCarpet.style.display = "block";	  
	}
	btnCarpet3.onclick = function() {
	  modalSofa.style.display = "none";
	  modalCarpet.style.display = "block";	  
	}
	btnSofa.onclick = function() {
	  modalSofa.style.display = "block";	  
	}	
	btnSofa2.onclick = function() {
	  modalCarpet.style.display = "none";
	  modalSofa.style.display = "block";	  
	}
	btnSofa3.onclick = function() {
	  modalFloor.style.display = "none";
	  modalSofa.style.display = "block";	  
	}
	btnFloor.onclick = function() {
	  modalFloor.style.display = "block";	  
	}	
	btnFloor2.onclick = function() {
	  modalMattress.style.display = "none";
	  modalFloor.style.display = "block";	  
	}
	btnFloor3.onclick = function() {
	  modalSofa.style.display = "none";
	  modalFloor.style.display = "block";	  
	}


	spanContact.onclick = function() {
	  modalContact.style.display = "none";
	}
	spanMenu1.onclick = function() {
	  modalMenu.style.display = "none";
	}
	spanMenu2.onclick = function() {
	  modalMenu.style.display = "none";
	}
	spanMenu3.onclick = function() {
	  modalMenu.style.display = "none";
	}
	spanMenu4.onclick = function() {
	  modalMenu.style.display = "none";
	}
	spanMenu5.onclick = function() {
	  modalMenu.style.display = "none";
	}	
	spanMenu6.onclick = function() {
	  modalMenu.style.display = "none";
	}		
	spanMattress.onclick = function() {
	  modalMattress.style.display = "none";
	}
	spanAir.onclick = function() {
	  modalAir.style.display = "none";
	}
	spanCurtain.onclick = function() {
	  modalCurtain.style.display = "none";
	}
	spanCarpet.onclick = function() {
	  modalCarpet.style.display = "none";
	}
	spanSofa.onclick = function() {
	  modalSofa.style.display = "none";
	}
	spanFloor.onclick = function() {
	  modalFloor.style.display = "none";
	}

	window.onclick = function(event) {
	  if (event.target == modalContact) {
		modalContact.style.display = "none";
	  }
	  if (event.target == modalMenu) {
		modalMenu.style.display = "none";
	  }	  
	  if (event.target == modalMattress) {
		modalMattress.style.display = "none";
	  }	
	  if (event.target == modalAir) {
		modalAir.style.display = "none";
	  }	
	  if (event.target == modalCurtain) {
		modalCurtain.style.display = "none";
	  }		  
	  if (event.target == modalCarpet) {
		modalCarpet.style.display = "none";
	  }	
	  if (event.target == modalSofa) {
		modalSofa.style.display = "none";
	  }		
	  if (event.target == modalFloor) {
		modalFloor.style.display = "none";
	  }		    	  	  	  
	}
</script>

<script>
function myFunction5() {
  window.open("https://urbanhygienist.com/img/Urban-Hygienist-Free-Mattress-Cleaning-eVoucher.pdf", "_blank", "Download");
}
</script>



 
<!-- CSS -->
<style>
.food-gif{
	width:100px;
	position:absolute;
	top:calc(50% - 150px);
	text-align:center;
}
.center-food{
	width:100%;
	text-align:center;
	margin-left:-50px;}
#container{
	margin-top:-20px;}
#overlay{
  position:fixed;
  z-index:99999;
  top:0;
  left:0;
  bottom:0;
  right:0;
  background:#7cd1d1;
  /*background: -moz-linear-gradient(left, #a9151c 0%, #d60d26 100%);
  background: -webkit-linear-gradient(left, #a9151c 0%,#d60d26 100%);
  background: linear-gradient(to right, #a9151c 0%,#d60d26 100%);*/
  transition: 1s 0.4s;
}
#progress{
  height:1px;
  background:#fff;
  position:absolute;
  width:0;
  top:50%;
}
#progstat{
  font-size:0.7em;
  letter-spacing: 3px;
  position:absolute;
  top:50%;
  margin-top:-40px;
  width:100%;
  text-align:center;
  color:#fff;
}

@media all and (max-width: 500px){
.food-gif{
	width:60px;
	top:calc(50% - 120px);
	text-align:center;
}
.center-food{
	margin-left:-30px;}	

}
</style>
<!--gwendolynkoay.vidatech@gmail.com,sherry2.vidatech@gmail.com,kevinyam.vidatech@gmail.com,urbanhygienist@gmail.com-->
<?php

if( $_SERVER['REQUEST_METHOD'] == 'POST') {

    // EDIT THE 2 LINES BELOW AS REQUIRED
    $email_to = "urbanhygienisttelemarketer1881@gmail.com";
    $email_subject = "Contact Form via Urban Hygienist website";
 
    function died($error) 
	{
        // your error code can go here
		echo '<script>alert("We are very sorry, but there were error(s) found with the form you submitted.\n\nThese errors appear below.\n\n';
		echo $error;
        echo '\n\nPlease go back and fix these errors.\n\n")</script>';
        die();
    }
 
 
    // validation expected data exists
    if(!isset($_POST['name']) ||
		!isset($_POST['telephone'])) {
        died('We are sorry, but there appears to be a problem with the form you submitted.');       
    }
	
     
 
    $first_name = $_POST['name']; // required
	$telephone = $_POST['telephone']; //required
	$address = $_POST['address']; 
    $comments = $_POST['comments']; 
	$status = $_POST['status']; 
	$age = $_POST['age']; 
	$conditions = $_POST['conditions']; 
	$date = $_POST['date']; 
	
	//$error_message = '<script>alert("The name you entered does not appear to be valid.");</script>';
	//if($first_name == ""){
	//	echo $error_message;
	//}

    
 
    $string_exp = "/^[A-Za-z .'-]+$/";
 
  if(!preg_match($string_exp,$first_name)) {
    $error_message .= 'The name you entered does not appear to be valid.\n';
  }
 


 
  if(strlen($error_message) > 0) {
    died($error_message);
  }
 
    $email_message = "Form details below.\n\n";
 
     
    function clean_string($string) {
      $bad = array("content-type","bcc:","to:","cc:","href");
      return str_replace($bad,"",$string);
    }
 
    $email_message .= "Name: ".clean_string($first_name)."\n";
	$email_message .= "Telephone: ".clean_string($telephone)."\n";  
	$email_message .= "Address: ".clean_string($address)."\n";  
    $email_message .= "Status: ".clean_string($status)."\n";  
    $email_message .= "Age: ".clean_string($age)."\n"; 
    $email_message .= "Conditions: ".clean_string($conditions)."\n";  
    $email_message .= "Other Medical Conditions : ".clean_string($comments)."\n";
    $email_message .= "Preferred Date for Appointment: ".clean_string($date)."\n"; 

// create email headers
$headers = 'From: '.'urbanhygienist@gmail.com'."\r\n".
'Reply-To: '.'urbanhygienist@gmail.com'."\r\n" .
'X-Mailer: PHP/' . phpversion();
@mail($email_to, $email_subject, $email_message, $headers);  
echo '<script>alert("Thank you! We will be in contact shortly!")</script>';

?>
<!-- include your own success html here -->

<!--Thank you for contacting us. We will be in touch with you very soon.-->
<?php
 
}
?>



</body>
</html>