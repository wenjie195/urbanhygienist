<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/CompanySelection.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$userRows = getUser($conn," WHERE user_type =1 ");

// $statusDetails = getStatus($conn);
// $reasonDetails = getReason($conn);

$companyDetails = getCompanySelection($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://urbanhygienist.com/adminEditCompany.php" />
    <meta property="og:title" content="Edit Company | Urban Hygienist" />
    <title>Edit Company | Urban Hygienist</title>
    <link rel="canonical" href="https://urbanhygienist.com/adminEditCompany.php" />
	<?php include 'css.php'; ?>
</head>
<body class="body">


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<?php include 'adminSidebar.php'; ?>
<div class="next-to-sidebar">

    <!-- <h1 class="h1-title">Edit Company</h1> -->

    <h1 class="details-h1" onclick="goBack()">
    	<a class="black-white-link2 hover1">
    		<img src="img/back.png" class="back-btn2 hover1a" alt="back" title="back">
            <img src="img/back2.png" class="back-btn2 hover1b" alt="back" title="back">
            Edit Company
        </a>
    </h1>

    <div class="clear"></div>

    <?php
    if(isset($_POST['company_name']))
    {
    $conn = connDB();
    $companyDetails = getCompanySelection($conn,"WHERE company_name = ? ", array("company_name") ,array($_POST['company_name']),"s");
    ?>

    <form   action="utilities/editCompanyNameFunction.php" method="POST">

        <div class="input50-div">
            <p class="input-title-p">Company Name : <?php echo $companyDetails[0]->getCompanyName();?></p>
            <input class="clean tele-input" type="text" placeholder="New Company Name" id="edit_company_name" name="edit_company_name" required>        
        </div> 

        <div class="clear"></div>

        <input type="hidden" value="<?php echo $companyDetails[0]->getId();?>" id="company_id" name="company_id" readonly>        

        <button class="clean red-btn margin-top30 fix300-btn" name="submit">Submit</button>

    </form>

    <?php
    }
    ?>

</div>
<style>
.company-li{
	color:#019ee2;
	background-color:white;}
.company-li p{
	color:#019ee2;}
.company-li .hover1a{
	display:none;}
.company-li .hover1b{
	display:block;}
</style>
<?php include 'js.php'; ?>

<script>
function goBack() {
  window.history.back();
}
</script>

</body>
</html>