<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

include 'selectFilecss.php';
include 'js.php';

require_once('vendor/php-excel-reader/excel_reader2.php');
require_once('vendor/SpreadsheetReader.php');

if (isset($_POST["import"]))
{
  $allowedFileType = ['application/vnd.ms-excel','text/xls','text/xlsx','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];

  if(in_array($_FILES["file"]["type"],$allowedFileType))
  {
    $targetPath = 'uploads/'.$_FILES['file']['name'];
    move_uploaded_file($_FILES['file']['tmp_name'], $targetPath);
    $Reader = new SpreadsheetReader($targetPath);
    $sheetCount = count($Reader->sheets());
    for($i=0;$i<$sheetCount;$i++)
    {
      $Reader->ChangeSheet($i);
      foreach ($Reader as $Row)
      {

        $name = "";
        if(isset($Row[0])) 
        {
          $name = mysqli_real_escape_string($conn,$Row[0]);
        }
        $phone = "";
        if(isset($Row[0])) 
        {
          $phone = mysqli_real_escape_string($conn,$Row[1]);
        }
        $email = "";
        if(isset($Row[0])) 
        {
          $email = mysqli_real_escape_string($conn,$Row[2]);
        }
        $teleName = "";
        if(isset($Row[0])) 
        {
          $teleName = mysqli_real_escape_string($conn,$Row[3]);
        }

        if (!empty($name) || !empty($phone)|| !empty($email)|| !empty($teleName))
        {
          // $query = "INSERT INTO excel (name,phone,email) VALUES ('".$name."','".$phone."','".$email."') ";
          $query = "INSERT INTO excel (name,phone,email,tele_name) VALUES ('".$name."','".$phone."','".$email."','".$teleName."') ";
          $result = mysqli_query($conn, $query);
          if (! empty($result))
          {
              $query = "INSERT INTO customerdetails (name,phone,email,tele_name) VALUES ('".$name."','".$phone."','".$email."','".$teleName."') ";
              $result = mysqli_query($conn, $query);
              if (! empty($result))
              { 
                echo "<script>alert('Excel Uploaded !');window.location='../telemarket_update/uploadExcel.php'</script>";       
                // echo "<script>alert('Excel Uploaded !');window.location='../adTele/uploadExcel.php'</script>";     
              }
              else 
              {
                echo "<script>alert('Problem in Importing Excel Data !');window.location='../telemarket_update/uploadExcel.php'</script>";
                // echo "<script>alert('Problem in Importing Excel Data !');window.location='../adTele/uploadExcel.php'</script>";
              }      
          }
          else 
          {
            echo "<script>alert('Problem in Importing Excel Data !');window.location='../telemarket_update/uploadExcel.php'</script>";
            // echo "<script>alert('Problem in Importing Excel Data !');window.location='../adTele/uploadExcel.php'</script>";
          }
        }
      }
    }
  }
  else
  {
  echo "<script>alert('Invalid File Type. Upload Excel File.');window.location='../telemarket_update/uploadExcel.php'</script>";
  // echo "<script>alert('Invalid File Type. Upload Excel File.');window.location='../adTele/uploadExcel.php'</script>";
  }
}
?>

<!DOCTYPE html>
<html>
<head>
  <?php include 'meta.php'; ?>
  <meta property="og:url" content="https://urbanhygienist.com/uploadExcel.php" />
  <meta property="og:title" content="Import Data | Urban Hygienist" />
  <title>Import Data | Urban Hygienist</title>
  <link rel="canonical" href="https://urbanhygienist.com/uploadExcel.php" />
  <?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'adminSidebar.php'; ?>
<div class="next-to-sidebar"> 

<h1 class="h1-title">Import Excel File</h1>

  <div class="outer-container">
    <form action="" method="post" name="frmExcelImport" id="frmExcelImport" enctype="multipart/form-data">
      <label>Select File</label><br><input type="file" name="file" id="file" accept=".xls,.xlsx"><div class="clear"></div>
      <button type="submit" id="submit" name="import"  class="clean red-btn margin-top30 fix300-btn margin-left0">Submit</button>
      <div id="response" class="<?php if(!empty($type)) { echo $type . " display-block"; } ?>"><?php if(!empty($message)) { echo $message; } ?></div>
    </form>
  </div>
</div>
<style>
.import-li{
color:#019ee2;
background-color:white;}
.import-li p{
color:#019ee2;}
.import-li .hover1a{
display:none;}
.import-li .hover1b{
display:block;}
.uh-footer{
	display:none;}
</style>
<div class="clear"></div>
<?php include 'js.php'; ?>

<script type="text/javascript" src="js/modernizr.custom.js"></script>
<script type="text/javascript" src="js/jquery.dlmenu.js"></script>
		<script>
			$(function() {
				$( '#dl-menu' ).dlmenu({
					animationClasses : { classin : 'dl-animate-in-2', classout : 'dl-animate-out-2' }
				});
			});
    </script>
</body>
</html>