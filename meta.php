<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta property="og:type" content="website" />
<meta property="og:image" content="https://urbanhygienist.com/img/fb-cover.jpg" />
<meta name="author" content="Urban Hygienist">
<meta property="og:description" content="We provide a comprehensive range of professional allergy hygiene solutions and cleaning services from mattress cleaning to air treatment with award-winning German technology." />
<meta name="description" content="We provide a comprehensive range of professional allergy hygiene solutions and cleaning services from mattress cleaning to air treatment with award-winning German technology." />
<meta name="keywords" content="urban hygienist, urban, clean, cleanliness, hygienist, hygiene, hygienic, clean, clear, dust mites, allergens, eczema, allergic rhinitis, asthma, removing dust, dust mites, allergens, water filter technology, allergy-free environment, no more breathing problems, healthy, healthy home, mattress cleaning, room air cleaning, curtain cleaning, carpet cleaning, sofa, cleaning, floor cleaning, award winning german technology, certified PREVENTATIVE MEDICAL PRODUCT 
FOR ALLERGIC PERSONS, 10 YEAR GUARANTEE*, etc">
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '632927013883759'); 
fbq('track', 'PageView');
</script>
<noscript>
 <img height="1" width="1" 
src="https://www.facebook.com/tr?id=632927013883759&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->